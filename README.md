# IML_Project

L'objectif de notre projet est de présenter les différentes parties demandés : Machine learning et Robotique (principalement), en différent sous projet.
Chaque projet contient un readme, des images, le code source et répond aux différentes parties.

le premier projet de machine learning comprend une base de données des 10 premières minutes du jeu League of Legends (LOL). 
L'objectif étant ici de savoir quel éléments du jeu durant les 10 première minutes l'équipe doit se concentrer pour gagner une partie.

Le deuxième projet de Robotique créer des mouvements aléatoire pour le robot turtleBot via le fichier "projet" et les lignes de commande (Human) via le nom de fichier "move".

Projet réalisé par :
Joshua RAMOS GIQUEL
Quentin DUBREUIL



